What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?
	
In order to communicate in the best way you really need to evaluate and consider a lot of things. If you want to communicate in a rather understandable fashion for everyone to understand, you will need to consider what people would understand. You simply cannot write down syntax and think that your mother would understand it. So keep it simple and make it understandable for everyone.
But on the other hand if you were to create a more complex flowchart you would also have to consider many things. If you wrote down everything syntax-wise that is going on in your program, the flowchart would be long and complicated. You have to be precise and at the same time write down some more broader aspects of the program. E.g. write down that you create a canvas, but maybe not necessarily the size and color.

What are the technical challenges for the two ideas and how are you going to address them?

For the first idea, the chance game, it might be to make the game smooth and how we imagine it, with several rounds and animations of the cards flipping over. If we are not able to fix the issues, we could simply cut the animation of the cards and just make it appear right after clicking it.
For the second idea, the Ascii feature, we have not encountered any technical issues yet. But that is also based on the fact that the library we would use is working. That might be the biggest issue if not, or the text appearing on the screen. The text problem could be solved by looking at other peoples programs who have used the same, and for the library we could be forced to use something else.

	
What is the value of the individual and the group flowchart that you have produced?
	
In my case is my individual flowchart more algorithmic and more specific. I do believe it gives a better understanding of the program if you are looking at it as a programmer. While it is more specific it do not include syntax, so people without a programmer background would maybe have a chance to look at it and understand it.
The group flowcharts is way more understandable for everyone. It is short and precise and is written in a way everyone should be able to understand. We have focused a lot more on showing the idea rather than how the final program would look like. And the flowcharts is created with the same purpose in mind as we had for the ideas. That the programs we made we were in total control of and that it could only go one way, just as the flowchart is only going in one direction and have no weird pathways.



Individual flowchart (based on Miniex6):
![Screenshot](page_1.png)


Group flowchart:
![Screenshot](AP_Flowcharts.png)


