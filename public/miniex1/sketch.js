let c;
function setup() {
  // put setup code here
  createCanvas(900,680);
  background('red');
  c = color(255,204,0);
}
function draw() {

fill(c);
ellipse(450, 350, 500, 500);
noFill();
stroke(255, 102, 0);
stroke(100, 100, 100);
bezier(300, 70, 89, 40, 700, 1000, 1500, 800);
fill(0);
textSize(30);
textFont('sans-serif');
text('plutos lost brother', 450, 300);
textFont('sans-serif');
text('planet', 250, 500);

}
function mousePressed() {
  if (c === 'magenta') {
    c = color(255,204,0);
  } else {
    c = 'magenta';
  }
}
