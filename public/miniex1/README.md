How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?

- My first experience with coding and programming have been a joyful, yet challenging experience. It was filled with a lot of surprises and frustrations due to the lack of knowledge in the field, but fueled by the will and eager to lean this new language. I have leaned a lot upon P5.js references to try to decipher the coding and the meanings of all the words and numbers. During the course of creating my first miniex I feel like my ability to read and understand code have evolved a bit, and thinking about the future doesn't worry me.

How is the coding process different from, or similar to, reading and writing text?

- At first I felt like the process of reading and writing code was completely different from reading and writing standard text. But I started to compare to when I was learning English and Spanish for the first time. It was difficult at first, but then I started to learn some of the words and numbers, and then I could begin to connect the dots. I am still at baby-level coding, but I know I will be somewhat decent in the end.

What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?

- For me, programming has not mean anything to me in the past, but I have always felt like it was a very useful tool to master. The assigned reading have made me reflect upon literacy and it makes a lot of sense for me when learning code.




RUNME - https://madslindgaard.gitlab.io/ap-2020/miniex1/

Sketch - https://gitlab.com/MadsLindgaard/ap-2020/-/tree/master/public/miniex1

![Screenshot](miniex1pic.png)
