RUNME - https://madslindgaard.gitlab.io/ap-2020/miniex4/

-- Provide a title of your work and a short description of the work (within 1000 characters) as if you are submitting to the festival open call.
	
"The Modern Home - We Invite You And Your Data To Stay Forever". I made this artwork with the specific thought in mind to reflect upon the so called "smart homes" and their intake in the culture. The artwork is showing a smart home that looks a lot like the homes we know today, and more specific the doors we know from homes and hotels. The door is featuring a "peep hole" as we know, but instead of only being able to have a look out of it, it also records everyone that walks by and store their faces. In the end, the home would be able to recognize the people visiting, comparing the face to other faces in a database storing peoples faces and information, and unfold the data stored to the home owner. They would then be able to receive a message from the home telling them whether or not it is a familiar face and the name of the person.
	
-- Describe your program and what you have used and learnt.
	
I would describe my program as super simple. The syntax is almost at a bare minimum, and the scene is set by a picture made by myself in Photoshop. What I have learned in this week is the implementation of the webcam. At first I struggled with the merging of a background image and the webcam, as the webcam could not display if the background was displaying and vice versa. But after trying to fiddle with the syntax I was able to spot the mistake in some of the placement syntax. The text is simply inserted by the help of the "text" reference in P5.js references.
	
	
-- Articulate how your program and thinking address the theme 'capture all'.
	
In my opinion do the program fit perfectly in to the theme of "capture all". Because that is exactly what it does. It captures all the people walking by and the storing the information. It would be able to name and describe everyone visiting or walking by the house via the connection to a larger database of faces and information. The thought behind the artwork was as well to show that even the most simple and well known things from our everyday life is at some point capable of taking control of our data. Without us even knowing it.
	
-- What are the cultural implications of data capture?
	
The increase in data capturing is very much two sided as I see it. On one side it is helping the authorities to locate wanted people or spot any suspicious activity. Besides that is all of the feeds on our social media build by the data we give to the provider, by choice or not.
And on the other hand it is  terrifying to know that all the information about us is stored somewhere without us knowing it. And it can be used against us at any time.
I suspect it will create a small "digital world war" with people trying to obscure the companies in their data gathering. We have already seen people trying to take matters in their hands and I recon it will only explode from now on.


Sketch: https://gitlab.com/MadsLindgaard/ap-2020/-/blob/master/public/miniex4/sketch.js





![Screenshot](miniex4.png)

