
let bg;

function setup() {
    //Loading of the background image
  bg = loadImage('pics/door copy.png');
  createCanvas(800,600);

    //Starting the webcam and making it fit the canvas
  let c = createCanvas(800, 600);
  c.position(0,0);
  let capture = createCapture();
  capture.size(60,60);
  capture.position(380,260);




}

function draw() {
    //Inserting the background image
    background(bg);

    //Making the upper text
    fill(0);
    textSize(32);
    textFont('Garamond');
    text('The Modern Home', 280, 100);
   
    //Making the lower text 
    fill(0);
    textSize(20);
    textFont('Garamond');
    text('I Record Everyone That Walks By', 269, 160);






}
