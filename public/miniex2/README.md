- Describe your program and what you have used and learnt. 

My program consists of two emojis. One is depicting a head and the other one a flag. More specifically a head looking like Adolf Hitler and the Nazi flag. This might be one of the most controversial emojis made in this class, and that was exactly what I was looking for.
The program is not that advanced and I have most of all just used circle and lines. For the moustache and the flag I have used a rectangle , and for the hair of the emoji I have used an arc.
The most important thing I have learned in writing this program was the ability to implement a picture. The swastika in the flag is an image I downloaded from the internet, cropped it and made it fit in to the white circle.



- How would you put your emoji into a wider cultural context that concerns representations, identity, race, social, economics, culture, device politics and beyond? (Try to think through the assigned reading and your coding process, and then expand that to your experience and thoughts - this is a difficult task, you may need to spend sometimes in thinking about it).

As mentioned, my emojis is rather controversial and they made me question my choice of emoji many times during the process. At the same time I convinced myself that while I might find it controversial someone in the world would acknowledge it and have no issues in using them. That being for example neo-Nazis and people making fun of it.
I recon these emojis would raise a lot of discussion and make many people angry, but I believe this topic surrounding the Nazis and their regime is an important discussion to have. At one hand I believe it is important to acknowledge the history and the events that have taken place, even though they are not joyful or happy. This time during history is one of the worst and most gruesome to ever have taken place, but we can never neglect it happened. On the other hand it effected so many people and still does to this day, so why do they have to be kept reminded about it?
During the time of emojis, a lot of people have let their voices be heard about the lack of diversity in the Unicode line-up, and I would imagine a tiny amount of the population would like to see my two emojis enter the line-up as well. So would the Unicode Consortium ever let them in? Most definitely not. The users longing for these kind of emojis are way to few to have any kind of influence, and at the same time it is way to controversial 

Just to be clear, I highly dissociate myself from these two emojis and what they represent. It do not depict my own thoughts and is made purely to be controversial.



RUNME - https://madslindgaard.gitlab.io/ap-2020/miniex2/

Repository: https://gitlab.com/MadsLindgaard/ap-2020/-/tree/master/public/miniex2

![Screenshot](emoji.png)

