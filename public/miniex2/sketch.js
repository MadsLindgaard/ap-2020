let img;
function preload() {
  img = loadImage('pics/swastika.png');

}
function setup() {
  createCanvas(windowWidth,windowHeight);

}

function draw() {

  background(255)


  //head
  noStroke();
  fill("orange")
  circle(170, 160, 300);
  //eye
  fill("blue")
  circle(100,130,30);
  //pupil
  fill("black")
  circle(100,130,10);
 //eye
  fill("blue")
  circle(230,130,30);
  //pupil
  fill("black")
  circle(230,130,10);
  //moustache
  fill(0);
  rect(143, 190, 40, 30);
  //mouth
  strokeWeight(5);
  stroke(0);
  line(145, 250, 185, 250);
  //hair
  fill(0);
  arc(200, 30, 200, 60, 0, PI + QUARTER_PI, CHORD);

  //background of flag
  noStroke();
  fill("red")
  rect(400,30,410,270);

  //circle in flag
  fill(255);
  circle(570,170,180);

  //swastika
  image(img, 438,86, 300, 180);
  console.log('pic');

  }
