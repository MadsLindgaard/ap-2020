RUNME - https://madslindgaard.gitlab.io/ap-2020/miniex3/

Repository - https://gitlab.com/MadsLindgaard/ap-2020/-/tree/master/public/miniex3

• **What is your sketch? What do you want to explore and/or express?**

My sketch is two throbbers depicting the same thing: waiting for something to load. But with two different meanings. At one hand it is an ode to the contemporary digital culture where we all, often, wait for the page to load something that gives us enjoyment. This being shown by the use of playful colors and more specifically the Google colors.
	On the other hand it loads our existentiality and for the rest of our lives. We might wait for enjoyment in the moment, but we wait forever for the life to finish loading. We always evolve and progress in the life. I imagine the life as a progress/loading bar going for 100% up until our death.
	
• **What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your process of coding)?**
	
The syntax of this program do not include rotation or anything else we have learnt this week. It consists of two arcs changing sizes. But that motion is simulating a movement in the throbber, because the two arcs are chasing each other. And exactly that is my time-related syntax. The two arcs chasing each other is a metaphor for time. Time is as well chasing. This second is chasing the next second. Everything is chasing the next in order to progress and move forward. With the sheer goal of ending in something.
	The computer do not understand time and the throbber is nothing to it. It is something we gave a symbolic value, but the throbber itself is not doing anything for the computer. This program is as well depicting the difference between humans and computers. We are waiting for something to load and we have the expectation of the thing to load at some point. But the computer can not differentiate between time, so it might just load forever without feeling some kind of emotion connected to waiting.
	
• **Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a ticket transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?**

I encounter a throbber everyday in my digital life, and to me it communicates the computer is processing and rendering the page, video or song I want to see/hear. It hides all the technicalities going on in the software inside the hardware, because the most human beings are not capable of processing and understanding what is going on. Besides that it is going so fast we would not be able to even see it.
We could characterize the icon as a hourglass or a clock. We associate these two things with time passing and waiting for something.

I got the inspiration for the trobber from this website. I fully understand the code and was able to modify it myself and make it fit my liking https://creativecoding778.wordpress.com/

The background is just an image i made myself in Photoshop saying "L ading - f rever". I now fully understand how to implement a background image with the help from https://p5js.org/examples/image-background-image.html

![Screenshot](loading_forever.png)
