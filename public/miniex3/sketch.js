var r1 =0;
var r2 =1;
var r3 =0;
var r4 =1;
let bg;

function setup(){
  bg = loadImage('pics/loading copy.png');
  createCanvas(900,675);

  frameRate(50)
}
function draw(){

//the large throbber in loading

  //background picture I made myself
  background(bg);

  //wight of arc
  strokeWeight(10);

  //the color of the first arc
  stroke(255);

  //the first arc
  arc(218,355,90,90,0,0);

  //the color of the second arc
  stroke(800,0,100);

  //the second arc
  arc(218,355,90,90,r1,r2);

  r2=r2+1/19;

  if (r2==6.5){
      r2=r2+1;
  }
  r1=r1+1/20;

//the small throbber in forever
  strokeWeight(4);

  //the color of the first arc
  stroke(255);

  //the first arc
  arc(388,478,25,25,0,0);

  //the color of the second arc
  stroke(0);

  //the second arc
  arc(388,478,25,25,r3,r4);

  r4=r4+1/19;

  if (r4==6.5){
      r4=r4+1;
  }
  r3=r3+1/20;
}
