let data;
let img;

function preload() {

// Loading the JSON file
data = loadJSON("encyclopedia.json");

// Loading the header for the page
img = loadImage ('pics/enc.png');

}


function setup() {

// Creating a Canvas at the very top of the screen for our header
createCanvas (1460,100);

// Placing the header
image (img, 0,0);



// Connecting the JSON file to the sketch and loading the large array
let facts = data.encyclopedia;

// For loop for listing the headlines of the different topics
for (let i = 0; i < facts.length; i ++) {
  createElement('h2', facts[i].topic);

// This creates the ability to show all the different smaller arrays
let content = facts[i].content;

// For loop for listing all the content of the topics
  for (let j = 0; j < content.length;j++){
    createDiv(content[j]);
  }

}




}

  function draw () {


  }
