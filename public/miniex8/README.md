RUNME - https://madslindgaard.gitlab.io/ap-2020/miniex8/

SKETCH - https://gitlab.com/MadsLindgaard/ap-2020/-/blob/master/public/miniex8/sketch.js

**CREATED IN COLLABORATION WITH MIKKEL HEINRICH**

• Provide a title of your work and a short description of the work (within 1000 characters).
	
The title of our work is "Lindgaard & Heinrich's Encyclopedia". As the title says it is an encyclopedia and more specific an encyclopedia of random and maybe useless facts. The idea to make an encyclopedia came to mind when thinking about JSON files and how they store a huge amount of information and data. And what other things does that? Encyclopedias. But instead of having a large database sorted alphabetically and by order we wanted to make something weird and different. Ranging from Asian currencies to bad pickup lines this encyclopedia consist of a lot of random things. And the purpose is as well to create a larger database and encourage people to fill it out themselves or gives us inspiration to expand the encyclopedia with new and random knowledge.
	
	
• Describe your program in terms of how it works, and what you have used and learnt?
	
The program consists of a large JSON file of over 750 lines of code and a small .js file. The JSON file consists of all the different topics of the encyclopedia and the content connecting to those topics. The .js file retrieve the data from JSON and list them rather as black text on a white canvas. As the program is now running and functioning it is very easy to add new information, by simply entering the JSON file and typing whatever you'd like.
Before we made the encyclopedia we had a lot of different ideas and several of them were more "crazy" than what we ended up with. The reason for that was that we struggled a lot to get our JSON and .js files to connect and it drove us near the edge of insanity in the end. So by following "10.3: What is JSON? Part II - p5.js Tutorial" by Daniel Shiffman on YouTube we ended up with a working template for making this program.
Inspiration: https://www.youtube.com/watch?v=118sDpLOClw 
	
	
• Analyze and articulate your work: 
	
To me, this program is not very artistic or performative. It do not include double meaning of any code and there are no hidden lines of text that do not show up in the final program. But that was never the plan for this program. We might have used the idea of an encyclopedia because we struggled to get other things to work, and we might not have "decorated" it good enough, but in the end that was a part of the whole idea. We figured that we had to use what we had and make the best of it. So that is why it do not include a lot of colors or index, simply because we wanted to make a "boring" and old encyclopedia only made for reading purpose and to gain knowledge. Wikipedia is not shiny and smart, is it?
By having more time or, simply just have reached out for help, we would maybe have been able created something cooler, but we do really find this program stupid and funny and at the same time somewhat useful.

![Screenshot](encc.png)
