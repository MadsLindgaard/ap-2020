
//Array of words, created by a random generator, for the program to choose from
let noun = ["director", "perspective", "world", "maintenance", "vehicle", "poem", "priority", "guidance", "committee", "application", "confusion", "police", "worker", "indication", "mom"];

let verb = ["balances", "overcomes", "lacks", "represents", "reinforces", "awaits", "births", "omits", "captures", "supresses", "stretches", "achieves", "accesses", "parts", "embodies"];

let adjective = ["adventurous" , "ordinary", "desperate", "spectacluar", "shallow", "wet", "consistent", "first", "equable", "sincere", "scrawny", "vague", "maniacal", "quarrelsome", "political"];

function setup() {
  createCanvas(800, 800);
  frameRate(5);


let ord1 = random(adjective);
let ord2 = random(noun);
let ord3 = random(verb);
let ord4 = random(noun);

//Creating a random sentence with the use of random words from the array
  for(i = 0; i < 800; i = i + 100){

    fill(0);
    textSize(20);
    text(ord1, 30,i);
    text(ord2, 180,i);
    text(ord3, 330,i);
    text(ord4, 480, i);
  }

}

function draw() {
noFill()

//Creating the random pattern of ellipses and rectangles
  for(i = 0; i <= 20; i++){
   if(random(1) < 0.5){
      ellipse(random(800), random(800), random(5,40));
              } else {
        rect(random(800), random(800), random(5,40), random(5,40));
             }

   }


}
