RUNME - https://madslindgaard.gitlab.io/ap-2020/miniex7/

**CREATED IN COLLABORATION WITH JONAS DITLEVSEN**

• What are the rules in your generative program and describe how your program performs over time and how are the running of the rules contingently enabled emergent behaviors?

The program is based upon two different rules. The first rule is for the program to either make an ellipse or a rectangle. 50% chance of making each and making them in different sizes.
The second rule is for the program to create a sentence of four different words, which is random words collected from three different arrays. Every time the program starts or reload a new sentence will emerge.
These rules will create a sentence and different ellipses and rectangles upon that. The sentence will stay in "the background" as the ellipses and rectangles will keep emerging on top of each other, so they eventually create layers upon layers of these figures and the text would not be visible anymore.

• What's the role of rules and processes in your work?

The rules is what is making our program. Without these two rules we would not have a generative program. But the most important part of the program might be the ellipses and rectangles, as they fill up the screen the most and they really show how the program makes random arts over and over again. The text is important for making a more artistic program as it generates a sentence based on random words and then disappear.

• Draw upon the assigned reading(s), how does this mini-exericse help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter?

This weeks readings and the creation of the miniex really shows that randomness is never fully random. In order for something to be random in computing you will have to setup rules and tell the computer what to do. You might use the function random, but that is you telling the computer to choose something from a predefined selection. You never know which one it chooses next, but you know from what category it chooses it from. Generative art is cool and is able to create some amazing and diverse art pieces every time it runs, but by reading the source code you already have an idea of the things it uses to create this artwork.

![Screenshot](Miniex7.png)
