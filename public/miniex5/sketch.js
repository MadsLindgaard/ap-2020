let mask;
let corona;


function preload() {

  //To load the mask at first when running the program
  mask = loadModel('mask/mask.obj');

  //As well with the text
  corona = loadFont('mask/corona.otf');

}

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);

  //Creating the text font I downloaded online
  textFont(corona);

  //Setting the size for the text
  textSize(2);



}

function draw() {

  background(255,255,0);

  //Giving the user the ability to move and zoom in and out on the 3D object
  orbitControl();

  //Sizing the mask
  scale(30);

  //Making the mask rotate
  rotateY(frameCount * 0.02);

  //Giving color to the mask
  normalMaterial();

  //Loading the mask
  model(mask);

  //Color of the text
  fill(0);

  //Loading the text
  text('Loading Corona',0,0);

  //Making the text align with the center of the screen
  textAlign(CENTER);



}
