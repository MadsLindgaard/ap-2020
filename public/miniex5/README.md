RUNME - https://madslindgaard.gitlab.io/ap-2020/miniex5/

For this miniex i wanted to try something completely new for me, and not necessarily redo one of my earlier programs. Yet the focus of this miniex was to make another program fit under the theme of "throbber". So this is not a reimagination of my old design, but just another take on the task.

My concept of this program was to make something that had a relation to current time, and more specifically the themes that surround us today. And what is more relevant than the corona virus? The outbreak of the virus has sparked a fear in many people, and many of them fear for their own well being and that they will eventually one day get the virus. So this throbber is supposed to show their mindset and that it is, apparently inevitable, that we will all catch the virus one day. 
The program is made with a large sense of irony and the text font and mask is supposed to show that. It is kind of cartoonish and the mask is in different bright colors.
I primarily want to express that a lot of people should relax and try to listen to the doctors that knows more about it than we do. We are not all going to die because of this virus. 

My syntax consists of very few things. The main part of the program is the insertion of a 3D mask and a text. But for me this was something new and I was definitely not able to do this when I started programming. Furthermore you are able to rotate and zoom in and out of the mask with the use of your mouse. Left clicking and moving around spins the object, and right clicking and moving around allows you to move the object to different places. Last but not least you can zoom in and out on the page to zoom in and out on the object. This is made by using the " orbitControl();" syntax.

For me aesthetic programming is the ability to make something that can load on a webpage. I do not necessarily have to be pretty but it is a way for the programmer to create and envision some of his/hers ideas. It gives you the freedom of being able to make what ever you want, and the ability to understand the syntax made by other people.
Digital culture would not be the same without programming. Who would make the platforms? There would not be any social media, dating sites or whatever fills up the internet, if we did not have anyone to make these sites. Everything digital is in the end programmed.

Inspiration for the mask: https://www.turbosquid.com/Search/3D-Models


Inspiration for coloring the mask: https://github.com/processing/p5.js/wiki/Getting-started-with-WebGL-in-p5#lights-and-materials

![Screenshot](coronaload.png)

