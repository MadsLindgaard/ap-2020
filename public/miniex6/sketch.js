let screen = 0;
let y=-20;
let x=200;
let speed = 2;
let score= 0;
let img;
let img2;
let img3;

function preload() {

  //Preloading all the images in my program
  img = loadImage('pics/dd.png');
  img2 = loadImage('pics/mex.png');
  img3 = loadImage('pics/tt.png');

}

function setup() {
  createCanvas(900,600);

  //This removes the cursor so it looks like you are only moving Donald Trump
  noCursor();
}

function draw() {

  //Creating the different screens of the game with if and else
	if(screen == 0){
    startScreen()
  }else if(screen == 1){
  	gameOn()
  }else if(screen==2){
  	endScreen()
  }
}

  //Creating the starting screen with a seperate function
function startScreen(){
		background(255, 255,0)
		fill(0)
		textAlign(CENTER);
    image(img3,10,10);
		text('THE BORDER CONTROL GAME',700,200)
		text('CLICK TO START CATCHING MEXICANS', 700,240);
		reset();
}
  //Creating the game screen itself
function gameOn(){
	background(0,128,0)
  fill(255)
  text("MEXICANS = " + score, 50,20)
  image(img2,x,y,40,40)
  imageMode(CENTER);
  image(img,mouseX,height-30,80,80)
	y+= speed;
  if(y>height){
  	screen =2
	 }
  if(y>height-10 && x>mouseX-20 && x<mouseX+20){
  	y=-20
    speed+=.5
    score+= 1
  }
	if(y==-20){
  	pickRandom();
  }
}

  //Making sure the mexican flags drop in different places
function pickRandom(){
	x= random(20,width-20)
}

  //Creating the ending screen
function endScreen(){
		background(0)
		textAlign(CENTER);
		text('THEY GOT OVER THE BORDER', width / 2, height / 2)
  	text("MEXICANS = " + score, width / 2, height / 2 + 20)
		text('click to catch more', width / 2, height / 2 + 40);
}

  //Making the game reset if you press the mouse
function mousePressed(){
	if(screen==0){
  	screen=1
  }else if(screen==2){
  	screen=0
  }
}
  //Resetting the game
function reset(){
	  score=0;
  	speed=2;
  	y=-20;
}
