RUNME - https://madslindgaard.gitlab.io/ap-2020/miniex6/

My game is supposed to display a problem in the world today, and for some reason the "Mexico vs. Trump battle" came to mind. It depicts Donald Trump trying to catch the illegal immigrants trying to get in to the US. He does that by the player moving the mouse to control Donald Trump and he then has to "catch" the Mexican flags falling down from above. The flags starts to fall down faster and faster for everyone he catch until he at some point is not able to catch them anymore and they cross the border. You are then able to start over again and try to beat your score.

The game actually came to mind before I even started coding this week and I knew I was trying to make this style of game. I searched for inspiration online and found a person that created the basis of this program. So I, granted, took a lot of inspiration from that program and made it fit my liking. I feel no regret in doing so, as I am able to fully understand the syntax and being able to tweak it and still make it work.

Due to my illness earlier this week and the circumstances we live under in Denmark today I really have not been able to fully understand and master the subject of object-oriented programming. The mixture of a difficult topic and not being able to attend the necessary lecture has put me back. Therefore this program might not include the needed syntax for this weeks topic.

![Screenshot](miniex6.png)

Sketch: https://gitlab.com/MadsLindgaard/ap-2020/-/blob/master/public/miniex6/sketch.js